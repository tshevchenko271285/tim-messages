/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

window.timScroll = function(to){
    let y = window.scrollY,
        timer;
    timer = setInterval(()=>{
        if( y <= to ) {
            y = y+5;
            window.scrollTo(0, y);
            if( y >= to )
                clearTimeout(timer);
        } else {
            y = y-5;
            window.scrollTo(0, y);
            if( y <= to )
                clearTimeout(timer)
        }
    }, 1)
};

Vue.component('tim-messages', require('./components/messages/TimMessagesComponent.vue').default);
Vue.component('tim-message', require('./components/messages/TimMessageComponent.vue').default);
Vue.component('tim-write-message', require('./components/messages/TimWriteMessageComponent.vue').default);

export const eventEmiter = new Vue();
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
