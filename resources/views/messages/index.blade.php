@extends('layouts.app')

@section('content')
    <div class="container">
        @auth()
            <tim-write-message></tim-write-message>
        @endauth
        @guest()
            <div class="row justify-content-center">
                <a href="{{route('loginGoogle')}}" class="login-button">Google+</a>
            </div>
            <div class="row justify-content-center">
                * Для добавления и коментирования сообщений выполните вход
            </div>
        @endguest
        <tim-messages></tim-messages>
    </div>
@endsection
