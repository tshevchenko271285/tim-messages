@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <a href="{{route('loginGoogle')}}" class="login-button">Google+</a>
        </div>
    </div>
@endsection
