<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Message;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( $request->ajax() ) {
//            $messages = Message::where('parent_id', null)->orderBy('id', 'desc')->paginte(5);
            $query = Message::query();
            $query->where('parent_id', null)->orderBy('id', 'desc');
            $messages = $query->paginate(5);
            return $messages;
        }
        return view('messages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( !Auth::check() ){
            return response('Unauthorized', 401);
        }
        $text = $request->input('text');
        $user_id = Auth::id();
        $message = Message::create([
            'user_id' => $user_id,
            'text' => $text,
        ]);
        if( $request->has('parent_id') && $request->input('parent_id') ) {
            $message->parent_id = $request->input('parent_id');
        }
        $message->save();
        return $message;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if( !Auth::check() ){
            return response('Unauthorized', 401);
        }
        $message = Message::find($id);
        $message->text = $request->input('text');
        $message->save();
        return $message;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getChildrenMessages(Request $request) {
        $parent_id = $request->input('id');
        $query = Message::query();
        $messages = $query->where('parent_id', $parent_id)->paginate(5);
//        $messages = Message::where('parent_id', $parent_id)->get();
        return $messages;
    }

    public function getCurrentUserId() {
        if( Auth::check() ) {
            return Auth::id();
        } else {
            return null;
        }
    }
}
