<h1>Tim Messages</h1>
<h2>Инсталяция приложения:</h2>
<ul>
    <li>Качаем репозиторий</li>
    <li><b>cmd:</b> composer install</li>
    <li><b>cmd:</b> npm install</li>
    <li>Переименуйте файл .env.example в .env</li>
    <li>В файле .env заполнить поля(APP_URL, DB_DATABASE, DB_USERNAME, DB_PASSWORD и т.д.)</li>
    <li>Зарегистрировать приложение в Google Console и заполнить соответствующие поля в файле .env(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, GOOGLE_CLIENT_CALLBACK)</li>
    <li><b>cmd:</b> php artisan key:generate</li>
    <li><b>cmd:</b> php artisan migrate</li>
</ul>
