<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if( Auth::check() ) {
        return redirect(route('messages.index'));
    }
    return view('index');
})->name('index');

Route::get('messages/getCurrentUserId', 'MessageController@getCurrentUserId');
Route::get('messages/childrens', 'MessageController@getChildrenMessages');
Route::resource('messages', 'MessageController');

Route::get('login/google', 'Auth\LoginController@redirectToGoogleProvider')->name('loginGoogle');
Route::get('login/google/callback', 'Auth\LoginController@handleGoogleProviderCallback');
Auth::routes();

